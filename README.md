## To run the server use the following commands:
***npm install***

## If you want to create a docker image use the following

***cd .\ForumService\\***

***docker build -t bnagy/forum-service .***

***docker run -p 4567:4567 -d bnagy/forum-service***

***cd ..\WeatherService\\***

***docker build -t bnagy/weather-service .***

***docker run -p 1426:1426 -d bnagy/weather-service***

***cd ..\MainService\\***

***docker build -t bnagy/main-service .***

***docker run -p 6789:6789 -d bnagy/main-service***

## Or just run ***docker-compose up***