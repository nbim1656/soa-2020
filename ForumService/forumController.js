var express = require('express');
var bodyParser = require('body-parser');
// invoke an instance of express application.
var app = express();

// initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/getforum', (req, res) => {
    var forumEntry = [{
            "senderName": "John",
            "message": "hello",
            "date": "2021-01-16",
            "time": "16:13"
        },
        {
            "senderName": "Lena",
            "message": "test message",
            "date": "2021-01-16",
            "time": "16:16"
        }
    ]
    res.send(JSON.stringify(forumEntry));
});

app.listen(4567);