# Level 1: System Context diagram 

```mermaid
graph TD;
    User-.->|" http " | ForumWebpage(Forum website system);
    ForumWebpage-.->|"Gets weather data based on the location"|openweathermap(Open weather map system)
```

# Level 2: Container diagram (Forum website system)

```mermaid
graph TD;
    User-.->|"visits localhost:6789(http)"|main-service
    main-service-.->|"Gets weather data based on the location(http)"|weather-service
    main-service-.->|"Gets forum data(http)"|forum-service
    weather-service-.->|"Gets weather data based on the location(http)"|openweathermap(Open weather map system)
```

# Level 3: Component diagram
```mermaid
graph TD;
    webBrowser(Web Browser)-.->|"localhost:6789/ or localhost:6789/login"|main-service
    main-service-.->|"provides"|SignIn(Sign in controller)
    SignIn-.->|"(uses)"|SessionController(Session controller)
    main-service-.->|"weather-service:1426/ (http)"|WeatherController(Location/Weather data controller)
    WeatherController-.->|"(uses)"|openweathermap(Open weather map system)
    main-service-.->|"forum-service:4567/ 
    (http)"|ForumController(Forum data controller)
```

# Level 4: Code

```mermaid
graph TD;
    webBrowser(Web Browser)-.->|"connects to"|main-service
    main-service-.->|"responds to"|webBrowser(Web Browser)
    main-service-.->|"localhost:6789/ or localhost:6789/login<br>provides"|public/index.html
    public/index.html-.->|"post request to ./login<br>if correct"|public/home.html
    public/home.html-.->|"provides data"|main-service
    main-service-.->|"weather-service:1426/:latitude/:longitude"|wsi(weather-service's index.js)
    wsi-.->|http://api.openweathermap.org/data/2.5/weather|Weatherdata(Weather data from 3th party api)
    Weatherdata-.->|"provides data"|wsi
    wsi-.->|provides data|main-service
    main-service-.->|"forum-service:4567/getforum"|fs(forum-service's index.js)
    fs-.->|"provides data"|main-service
```

# Used SOA patterns

* Microservice Architecture
* Service instance per Container
* Domain-specific protocol 