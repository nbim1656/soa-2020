var express = require('express');
var bodyParser = require('body-parser');
var fetch = require("node-fetch");
// invoke an instance of express application.
var app = express();

// initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/:latitude/:longitude', (req, res) => {
    console.log(req.params.latitude + " " + req.params.latitude);
    var supersecretAPIKey = "47ed4a2dc283134c35f93f674b0f4949";
    var URL = "http://api.openweathermap.org/data/2.5/weather?lat=" + req.params.latitude + "&lon=" + req.params.longitude + "&units=metric" + "&appid=" + supersecretAPIKey;
    fetch(URL).then(function(response) {
            return response.json();
        })
        .then(function(parsedData) {
            console.log(parsedData)
            var weatherdata = {
                "weathertype": parsedData.weather[0].main,
                "temperature": parsedData.main.temp,
                "city": parsedData.name
            }
            console.log(weatherdata);
            res.contentType('application/json');
            res.send(JSON.stringify(weatherdata));
        });
});

app.listen(1426);