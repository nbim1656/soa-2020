$(document).ready(function() {
    $(".logout").click(function(event) {
        event.preventDefault();
        var url = '/logout';
        var posting = $.post(url, {});
        posting.done(function(data) {
            if (data === 'done') {
                window.location.replace('/login');
            }
        });
    });
    $(".weatherdata").click(function(event) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }

    });
});

function showPosition(position) {
    var url = '/weatherdata' + "/" + position.coords.latitude + "/" + position.coords.longitude;
    var posting = $.get(url, {});
    posting.done(function(data) {
        var para = document.createElement("p");
        var h2 = document.createElement("h2");
        var node = document.createTextNode("Temperature:");
        var node1 = document.createTextNode("City: " + data.city);
        var node2 = document.createTextNode("Temperature: " + data.temperature);
        var node3 = document.createTextNode("Weather type: " + data.weathertype);
        var newLine1 = document.createElement("br");
        var newLine2 = document.createElement("br");
        h2.appendChild(node);
        para.appendChild(node1);
        para.appendChild(newLine1);
        para.appendChild(node2);
        para.appendChild(newLine2);
        para.appendChild(node3);
        var element = document.getElementById("weatherData");
        element.appendChild(h2);
        element.appendChild(para);
    });
}

function getforumData() {
    var url = '/getforum';
    var posting = $.get(url, {});
    posting.done(function(data) {
        console.log(data);
        data = JSON.parse(data);
        var h2 = document.createElement("h2");
        var node = document.createTextNode("Forum:");
        h2.appendChild(node);
        var element = document.getElementById("weatherData");
        element.appendChild(h2);
        for (var i = 0; i < data.length; i++) {
            var para = document.createElement("p");
            var node1 = document.createTextNode("Sender: " + data[i].senderName);
            var node2 = document.createTextNode("Message: " + data[i].message);
            var node3 = document.createTextNode("Date and time: " + data[i].date + " " + data[i].time);
            var newLine1 = document.createElement("br");
            var newLine2 = document.createElement("br");
            para.appendChild(node1);
            para.appendChild(newLine1);
            para.appendChild(node2);
            para.appendChild(newLine2);
            para.appendChild(node3);
            element.appendChild(para);
        }

    });
}
getforumData();