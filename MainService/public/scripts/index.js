$(document).ready(function() {
    $(".loginForm").submit(function(event) {
        event.preventDefault();
        var url = '/login';
        var username = $("input[name='uname']").val();
        var password = $("input[name='psw']").val();
        var posting = $.post(url, { 'uname': username, 'psw': password });
        posting.done(function(data) {
            var response = data.resp;
            if (response === 'ok') {
                window.location.replace('/home');
            } else {
                alert("Invalid password")
            }
        });
    });
});