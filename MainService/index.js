var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var fetch = require("node-fetch");
const redis = require('redis');
const morgan = require('morgan');
const router = express.Router();
const loginData = require('./secrets/userPass')
var app = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    cookieName: 'session',
    secret: 'random_string_goes_here',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
}));

app.get('/', (req, res) => {
    res.redirect('login');
});

app.get('/login', (req, res) => {
    if (req.session && req.session.uname) {
        res.redirect('home');
    }
    res.sendFile('public/index.html', { root: __dirname });
});

app.get('/pictures/:picture', (req, res) => {
    console.log(req.params.picture)
    res.sendFile('public/pictures/' + req.params.picture, { root: __dirname });
});

app.get('/styles/:style', (req, res) => {
    console.log(req.params.style)
    res.sendFile('public/' + req.params.style, { root: __dirname });
});

app.get('/scripts/:script', (req, res) => {
    console.log(req.params.script)
    res.sendFile('public/scripts/' + req.params.script, { root: __dirname });
});

app.post('/login', (req, res) => {
    console.log(req.body)
    if (req.body.uname === loginData.userName && req.body.psw === loginData.password) {
        req.session.uname = req.body.uname;
        res.send({ 'resp': 'ok' });
    } else {
        res.send({ 'resp': 'not ok' });
    }
});

app.get('/home', (req, res) => {
    if (req.session && req.session.uname) {
        res.sendFile('public/home.html', { root: __dirname });
    } else {
        res.redirect('login');
    }
});

app.post('/logout', (req, res) => {
    if (req.session && req.session.uname) {
        req.session.destroy();
        res.send('done');
    }
});

app.get('/weatherdata/:latitude/:longitude', (req, res) => {
    console.log("getting data from the weather service")
    var latitude = req.params.latitude;
    var longitude = req.params.longitude;
    console.log(latitude + " " + longitude);
    fetch('http://weather-service:1426/' + latitude + "/" + longitude).then(function(response) {
            return response.json();
        })
        .then(function(parsedData) {
            console.log("response: " + parsedData);
            res.send(parsedData);
            // if (!response.ok) {
            //     throw new Error(response.statusText)
            // } else {
            //     return response.json().then(({ saved }) => {
            //         return saved
            //     })
            // }
        })
});

app.get('/getforum', (req, res) => {
    console.log("getting data from the weather service")
    var latitude = req.params.latitude;
    var longitude = req.params.longitude;
    console.log(latitude + " " + longitude);
    fetch('http://forum-service:4567/getforum').then(function(response) {
            return response.text();
        })
        .then(function(parsedData) {
            console.log("response: " + parsedData);
            res.send(parsedData);
        })
});


app.listen(6789);