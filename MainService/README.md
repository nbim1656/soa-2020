## To run the server use the following commands:
***npm install***

***npm run serve***

## If you want to create a docker image use the following

***docker build -t bnagy/node-web-app .***

***docker run -p 6789:6789 -d bnagy/node-web-app***